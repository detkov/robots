package models;

import java.awt.*;
import java.awt.geom.AffineTransform;

import static java.awt.Color.MAGENTA;
import static models.Utilities.*;

public class Robot {
    public volatile double x;
    public volatile double y;
    public volatile double direction;
    public volatile boolean selected;

    public static final double maxVelocity = 0.2;
    public static final double maxAngularVelocity = 0.015;

    public static final int widthDiameter = 30;
    public static final int heightDiameter = 10;
    public static final int eyeDiameter = 5;

    public Robot(double x, double y, double direction, boolean selected){
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(){
        selected = true;
    }

    public void setUnselected(){
        selected = false;
    }

    public void moveRobot(double velocity, double angularVelocity) {
        double duration = 10;
        angularVelocity = applyLimits(angularVelocity, -Robot.maxAngularVelocity, Robot.maxAngularVelocity);
        double newX = x + velocity / angularVelocity *
                      (Math.sin(direction  + angularVelocity * duration) - Math.sin(direction));
        if (!Double.isFinite(newX))
            newX = x + velocity * duration * Math.cos(direction);
        double newY = y - velocity / angularVelocity *
                      (Math.cos(direction  + angularVelocity * duration) - Math.cos(direction));
        if (!Double.isFinite(newY))
            newY = y + velocity * duration * Math.sin(direction);
        x = newX;
        y = newY;
        direction = asNormalizedRadians(direction + angularVelocity * duration);
    }

    public void draw(Graphics2D g) {
        int robotCenterX = round(x);
        int robotCenterY = round(y);
        AffineTransform t = AffineTransform.getRotateInstance(direction, robotCenterX, robotCenterY);
        g.setTransform(t);
        fillAndDrawEllipse(g, robotCenterX, robotCenterY, widthDiameter, heightDiameter, MAGENTA);
        fillAndDrawEllipse(g, robotCenterX  + widthDiameter / 3, robotCenterY, eyeDiameter, eyeDiameter, Color.WHITE);
    }

    public boolean updateState(Point target) {
        if (target == null)
            return false;
        double distance = distance(x, y, target.x, target.y);
        if (distance < 1)
            return true;
        double angleToTarget = angleTo(x, y, target.x, target.y);
        double angularVelocity = 0;
        if (angleToTarget > direction)
            angularVelocity = Robot.maxAngularVelocity;
        if (angleToTarget < direction)
            angularVelocity = -Robot.maxAngularVelocity;

        if(Math.abs(angleToTarget - direction) < maxVelocity)
            moveRobot(maxVelocity, angularVelocity);
        else
            moveRobot(0, angularVelocity);
        return false;
    }

    public Point toPoint() {
        return new Point( round(x), round(y));
    }
}