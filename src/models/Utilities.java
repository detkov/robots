package models;

import java.awt.*;

public class Utilities {
    public static double distance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2)+ Math.pow(y1 - y2, 2));
    }

    public static double angleTo(double fromX, double fromY, double toX, double toY) {
        return asNormalizedRadians(Math.atan2(toY - fromY, toX - fromX));
    }

    public static double applyLimits(double value, double min, double max) {
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }

    public static double asNormalizedRadians(double angle) {
        while (angle < 0)
            angle += 2*Math.PI;
        while (angle >= 2*Math.PI)
            angle -= 2*Math.PI;
        return angle;
    }

    public static int round(double value) {
        return (int)(value + 0.5);
    }

    public static boolean isInsideEllipse(int x, int y, Robot robot){
        double shiftX = Math.pow(x - robot.x, 2);
        double shiftY = Math.pow(y - robot.y, 2);
        double divisionX = shiftX / Math.pow(Robot.widthDiameter / 2, 2);
        double divisionY = shiftY / Math.pow(Robot.heightDiameter / 2, 2);

        return divisionX + divisionY <= 1;
    }

    public static void fillAndDrawEllipse(Graphics g, int centerX, int centerY, int diam1, int diam2, Color inner){
        g.setColor(inner);
        g.fillOval(centerX - diam1 / 2, centerY - diam2 / 2, diam1, diam2);
        g.setColor(Color.BLACK);
        g.drawOval(centerX - diam1 / 2, centerY - diam2 / 2, diam1, diam2);
    }
}