package models;

public enum MenuItem {
    CreateRobot,
    RemoveRobot,
    SelectRobot,
    CreateObstacle,
    RemoveObstacle,
    NoneItem
}
