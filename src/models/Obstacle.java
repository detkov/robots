package models;

import java.awt.*;

public class Obstacle {
    public volatile Rectangle rect = new Rectangle();
    public Point leftUpper;
    public Point rightUpper;
    public Point rightLower;
    public Point leftLower;

    public Obstacle(int x, int y, int parity){
        this.rect.x = x;
        this.rect.y = y;
        if (parity % 2 == 0){
            this.rect.width = 100;
            this.rect.height = 150;
        } else {
            this.rect.width = 150;
            this.rect.height = 100;
        }
        leftUpper = new Point(rect.x - Robot.widthDiameter / 2, rect.y - Robot.widthDiameter / 2);
        rightUpper = new Point(rect.x + rect.width + Robot.widthDiameter / 2, rect.y - Robot.widthDiameter / 2);
        rightLower = new Point(rect.x + rect.width + Robot.widthDiameter / 2, rect.y + rect.height + Robot.widthDiameter / 2);
        leftLower = new Point(rect.x - Robot.widthDiameter / 2, rect.y + rect.height + Robot.widthDiameter / 2);
    }

    public void draw(Graphics2D g){
        g.setColor(Color.CYAN);
        g.fillRect(rect.x, rect.y, rect.width, rect.height);
        g.setColor(Color.BLACK);
        g.drawRect(rect.x, rect.y, rect.width, rect.height);

//        g.fillOval(leftLower.x, leftLower.y, 5, 5);
//        g.fillOval(rightLower.x, rightLower.y, 5, 5);
//        g.fillOval(leftUpper.x, leftUpper.y, 5, 5);
//        g.fillOval(rightUpper.x, rightUpper.y, 5, 5);
    }
}
