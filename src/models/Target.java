package models;

import java.awt.*;
import java.awt.geom.AffineTransform;
import static models.Utilities.*;

public class Target {
    public volatile int x;
    public volatile int y;

    public Target(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setPosition(Point p)
    {
        x = p.x;
        y = p.y;
    }

    public void draw(Graphics2D g)
    {
        AffineTransform t = AffineTransform.getRotateInstance(0, 0, 0);
        g.setTransform(t);
        fillAndDrawEllipse(g, this.x, this.y, 5, 5, Color.GREEN);
    }

    public Point toPoint(){
        return new Point(x, y);
    }
}