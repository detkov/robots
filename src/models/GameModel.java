package models;

import graph.Graph;
import graph.Dijkstra;
import graph.MultiList;
import javafx.util.Pair;
import log.Logger;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.util.ArrayList;

public class GameModel{
    public static Robot defaultRobot = new Robot(-100, -100, -100, false);
    private Obstacle defaultObstacle = new Obstacle(-1000, -1000, 0);

    private volatile MenuItem currentItem = MenuItem.NoneItem;
    public volatile boolean showGraph = false;
    private volatile Target target = new Target(200, 200);
    private volatile ArrayList<Robot> robots = new ArrayList<>();
    private volatile ArrayList<Obstacle> obstacles = new ArrayList<>();
    private volatile ArrayList<Point> nodes = new ArrayList<>();
    private volatile ArrayList<Pair<Line2D, Double>> edges = new ArrayList<>();
    private volatile ArrayList<Line2D> pathLines = new ArrayList<>();
    private volatile ArrayList<Point> pathPoints = new ArrayList<>();
    private volatile Point currentPointToReach;
    private volatile int currentPointToReachNumber;

    public GameModel(Target target, ArrayList<Robot> robots, ArrayList<Obstacle> obstacles){
        this.target = target;
        this.robots = robots;
        this.obstacles = obstacles;
        updateGraph();
        currentPointToReach = target.toPoint();
    }

    public GameModel(){
        robots.add(new Robot(100, 100, 0, true));
        updateGraph();
    }

    public void HandleClick(MouseEvent e){
        Point point = e.getPoint();
        switch (currentItem){
            case NoneItem:
                target.setPosition(point);
                nullifyGraph();
                Logger.debug("Target replaced");
                break;
            case CreateRobot:
                robots.add(new Robot(point.x, point.y, 0, false));
                Logger.debug("Created new Robot");
                break;
            case RemoveRobot:
                Robot robotToRemove = findRobot(point);
                if (!robotToRemove.equals(defaultRobot)){
                    robots.remove(robotToRemove);
                    Logger.debug("Robot removed");
                }
                break;
            case SelectRobot:
                Robot robotToSelect = findRobot(point);
                if (!robotToSelect.equals(defaultRobot)){
                    Robot previousSelectedRobot = getSelectedRobot();
                    if (!previousSelectedRobot.equals(defaultRobot))
                        previousSelectedRobot.setUnselected();
                    robotToSelect.setSelected();
                    nullifyGraph();
                    Logger.debug("New Robot selected");
                }
                break;
            case CreateObstacle:
                obstacles.add(new Obstacle(point.x, point.y, obstacles.size()));
                Logger.debug("Created new Obstacle");
                break;
            case RemoveObstacle:
                Obstacle obstacleToRemove = findObstacle(point);
                if (!obstacleToRemove.equals(defaultObstacle)){
                    obstacles.remove(obstacleToRemove);
                    Logger.debug("Obstacle removed");
                }
                break;
        }
    }

    public void updateRobotsState(){
        Robot robot = getSelectedRobot();
        if (robot == defaultRobot)
            return;
        if (robot.toPoint() == target.toPoint())
            return;
        boolean hasReached = robot.updateState(currentPointToReach);
        if (hasReached && currentPointToReachNumber + 1 < pathPoints.size()){
            currentPointToReachNumber++;
            currentPointToReach = pathPoints.get(currentPointToReachNumber);
        }
    }

    private void nullifyGraph() {
        updateGraph();
        currentPointToReachNumber = 0;
        currentPointToReach = !pathPoints.isEmpty() ? pathPoints.get(currentPointToReachNumber) : target.toPoint();
    }

    public void updateGraph() {
        Robot robot = getSelectedRobot();
        if (robot == defaultRobot){
            nodes = Graph.getGraphNodes(obstacles, robot, target.toPoint());
            edges = Graph.getGraphEdges(nodes, obstacles);
            pathPoints = new ArrayList<>();
            pathLines = new ArrayList<>();
            return;
        }
        nodes = Graph.getGraphNodes(obstacles, robot, target.toPoint());
        edges = Graph.getGraphEdges(nodes, obstacles);
        Dijkstra dijkstra = new Dijkstra(new MultiList(nodes, edges));
        pathPoints = dijkstra.dijkstraRMQ(0, nodes.size() - 1);
        currentPointToReach = currentPointToReachNumber < pathPoints.size() ? pathPoints.get(currentPointToReachNumber) : target.toPoint();
        pathLines = new ArrayList<>();
        for (int i = 0; i < pathPoints.size() - 1; i++)
            pathLines.add(new Line2D.Double(pathPoints.get(i), pathPoints.get(i + 1)));
    }

    public void changeItem(MenuItem menuItem) {
        currentItem = menuItem;
    }

    private void draw(Graphics2D g2d, GameObject object){
        switch (object){
            case Target:
                target.draw(g2d);
                break;
            case Robot:
                for (Robot robot: robots)
                    robot.draw(g2d);
                break;
            case Obstacle:
                for (Obstacle obstacle: obstacles)
                    obstacle.draw(g2d);
                break;
            case Node:
                for(Point node : nodes){
                    g2d.setColor(Color.RED);
                    g2d.fillOval(node.x, node.y, 5, 5);
                }
                break;
            case Edge:
                for (Pair<Line2D, Double> edge : edges){
                    Line2D edgeLine = edge.getKey();
                    g2d.setColor(Color.ORANGE);
                    g2d.draw(edgeLine);
                }
                break;
            case Route:
                for (Line2D line : pathLines){
                    g2d.setStroke(new BasicStroke(3));
                    g2d.setColor(Color.BLUE);
                    g2d.draw(line);
                    g2d.setStroke(new BasicStroke(1));
                }
                break;
        }
    }

    public void drawGameObjects(Graphics2D g2d) {
        draw(g2d, GameObject.Target);
        draw(g2d, GameObject.Obstacle);
        if (showGraph){
            draw(g2d, GameObject.Node);
            draw(g2d, GameObject.Edge);
            draw(g2d, GameObject.Route);
        }
        draw(g2d, GameObject.Robot);

    }

    private Robot findRobot(Point point) {
        for (Robot robot : robots) {
            if (Utilities.isInsideEllipse(point.x, point.y, robot)) {
                return robot;
            }
        }
        return defaultRobot;
    }

    private Obstacle findObstacle(Point point) {
        for (Obstacle obstacle : obstacles) {
            if (obstacle.rect.contains(point)) {
                return obstacle;
            }
        }
        return defaultObstacle;
    }

    public Target getTarget() {
        return target;
    }

    public ArrayList<Robot> getRobots(){
        return robots;
    }

    public ArrayList<Obstacle> getObstacles(){
        return obstacles;
    }

    private Robot getSelectedRobot(){
        for (Robot robot : robots)
            if (robot.isSelected())
                return robot;
        return defaultRobot;
    }
}
