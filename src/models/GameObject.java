package models;

public enum GameObject {
    Target,
    Robot,
    Obstacle,
    Node,
    Edge,
    Route
}
