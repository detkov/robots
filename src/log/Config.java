package log;

import gui.GameWindow;
import gui.LogWindow;
import models.GameModel;
import models.Obstacle;
import models.Robot;
import models.Target;

import javafx.util.Pair;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;

public class Config {
    public static void writeConfig(LogWindow log, ArrayList<GameWindow> games, ArrayList<GameModel> models) throws
            IOException, ParserConfigurationException, IllegalArgumentException, TransformerException, DOMException {
        checkConfigValidity();
        clearConfig();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.newDocument();

        Element windows = doc.createElement("Windows");
        doc.appendChild(windows);

        Element logWindowElement = createLogWindowElement(doc, log);
        windows.appendChild(logWindowElement);

        Element gameWindowsElement = createGameWindowsElement(doc, games, models);
        windows.appendChild(gameWindowsElement);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(doc);
        StreamResult file = new StreamResult(new File("config.xml"));
        transformer.transform(source, file);
    }

    private static Element createGameWindowsElement(Document doc, ArrayList<GameWindow> games, ArrayList<GameModel> models) {
        Element gameWindowsElement = doc.createElement("GameWindows");
        for (int i = 0; i < games.size(); i++) {
            GameWindow game = games.get(i);
            GameModel model = models.get(i);
            Element gameWindowElement = createGameWindowElement(doc, game, model);
            gameWindowsElement.appendChild(gameWindowElement);
        }
        return gameWindowsElement;
    }

    private static Element createGameWindowElement(Document doc, GameWindow game, GameModel model) {
        Element gameWindowElement = doc.createElement("GameWindow");
        gameWindowElement.appendChild(getNode(doc, "x", String.valueOf(game.getX())));
        gameWindowElement.appendChild(getNode(doc, "y", String.valueOf(game.getY())));
        gameWindowElement.appendChild(getNode(doc, "width", String.valueOf(game.getWidth())));
        gameWindowElement.appendChild(getNode(doc, "height", String.valueOf(game.getHeight())));
        Element gameModelElement = createGameModelElement(doc, model);
        gameWindowElement.appendChild(gameModelElement);
        return gameWindowElement;
    }

    private static Element createGameModelElement(Document doc, GameModel model) {
        Element gameModelElement = doc.createElement("GameModel");
        Element targetElement = createTargetElement(doc, model.getTarget());
        Element robotsElement = createRobotsElement(doc, model.getRobots());
        Element obstaclesElement = createObstaclesElement(doc, model.getObstacles());
        gameModelElement.appendChild(targetElement);
        gameModelElement.appendChild(robotsElement);
        gameModelElement.appendChild(obstaclesElement);
        return gameModelElement;
    }

    private static Element createObstaclesElement(Document doc, ArrayList<Obstacle> obstacles) {
        Element obstaclesElement = doc.createElement("Obstacles");
        for (Obstacle obstacle : obstacles) {
            Element obstacleElement = createObstacleElement(doc, obstacle);
            obstaclesElement.appendChild(obstacleElement);
        }
        return obstaclesElement;
    }

    private static Element createObstacleElement(Document doc, Obstacle obstacle) {
        Element obstacleElement = doc.createElement("Obstacle");
        obstacleElement.appendChild(getNode(doc, "x", String.valueOf(obstacle.rect.x)));
        obstacleElement.appendChild(getNode(doc, "y", String.valueOf(obstacle.rect.y)));
        obstacleElement.appendChild(getNode(doc, "width", String.valueOf(obstacle.rect.width)));
        obstacleElement.appendChild(getNode(doc, "height", String.valueOf(obstacle.rect.height)));
        return obstacleElement;
    }

    private static Element createRobotsElement(Document doc, ArrayList<Robot> robots) {
        Element robotsElement = doc.createElement("Robots");
        for (Robot robot : robots) {
            Element robotElement = createRobotElement(doc, robot);
            robotsElement.appendChild(robotElement);
        }
        return robotsElement;
    }

    private static Element createRobotElement(Document doc, Robot robot) {
        Element robotElement = doc.createElement("Robot");
        robotElement.appendChild(getNode(doc, "x", String.valueOf(robot.x)));
        robotElement.appendChild(getNode(doc, "y", String.valueOf(robot.y)));
        robotElement.appendChild(getNode(doc, "direction", String.valueOf(robot.direction)));
        robotElement.appendChild(getNode(doc, "selected", String.valueOf(robot.selected ? 1 : 0)));
        return robotElement;
    }

    private static Element createTargetElement(Document doc, Target target) {
        Element targetElement = doc.createElement("Target");
        targetElement.appendChild(getNode(doc, "x", String.valueOf(target.x)));
        targetElement.appendChild(getNode(doc, "y", String.valueOf(target.y)));
        return targetElement;
    }

    private static Element createLogWindowElement(Document doc, LogWindow log) {
        Element logWindowElement = doc.createElement("LogWindow");
        logWindowElement.appendChild(getNode(doc, "x", String.valueOf(log.getX())));
        logWindowElement.appendChild(getNode(doc, "y", String.valueOf(log.getY())));
        logWindowElement.appendChild(getNode(doc, "width", String.valueOf(log.getWidth())));
        logWindowElement.appendChild(getNode(doc, "height", String.valueOf(log.getHeight())));
        return logWindowElement;
    }

    private static Node getNode(Document doc, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

    public static Pair<Rectangle, ArrayList<Pair<Rectangle, GameModel>>> readConfig()
            throws IOException, SAXException, ParserConfigurationException {
        checkConfigValidity();
        File file = new File("config.xml");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(file);
        doc.getDocumentElement().normalize();
        Element windowsElement = doc.getDocumentElement();

        Element logWindowElement = (Element) windowsElement.getElementsByTagName("LogWindow").item(0);
        Rectangle logWindow = getWindowRectangle(logWindowElement);

        Element gameWindowsElement = (Element) windowsElement.getElementsByTagName("GameWindows").item(0);
        ArrayList<Pair<Rectangle, GameModel>> gameWindowsAndModels = getGameWindows(gameWindowsElement);

        return new Pair<>(logWindow, gameWindowsAndModels);
    }

    private static ArrayList<Pair<Rectangle, GameModel>> getGameWindows(Element gameWindowsElement) {
        ArrayList<Pair<Rectangle, GameModel>> gameWindowsAndModels = new ArrayList<>();
        NodeList gameWindowNodes = gameWindowsElement.getElementsByTagName("GameWindow");
        for (int i = 0; i < gameWindowNodes.getLength(); i++){
            Element gameWindowElement = (Element) gameWindowNodes.item(i);
            Rectangle gameWindowRectangle = getWindowRectangle(gameWindowElement);
            GameModel gameWindowModel = getWindowModel(gameWindowElement);
            gameWindowsAndModels.add(new Pair<>(gameWindowRectangle, gameWindowModel));
        }
        return gameWindowsAndModels;
    }

    private static GameModel getWindowModel(Element gameWindowNode) {
        Target target = getTarget(gameWindowNode);
        ArrayList<Robot> robots = getRobots(gameWindowNode);
        ArrayList<Obstacle> obstacles = getObstacles(gameWindowNode);
        return new GameModel(target, robots, obstacles);
    }

    private static ArrayList<Obstacle> getObstacles(Element gameWindowNode) {
        ArrayList<Obstacle> obstacles = new ArrayList<>();
        Element obstaclesElement = (Element) gameWindowNode.getElementsByTagName("Obstacles").item(0);
        NodeList obstacleList = obstaclesElement.getElementsByTagName("Obstacle");
        for (int i = 0; i < obstacleList.getLength(); i++){
            Element obstacleElement = (Element) obstacleList.item(i);
            Obstacle obstacle = getObstacle(obstacleElement);
            obstacles.add(obstacle);
        }
        return obstacles;
    }

    private static Obstacle getObstacle(Element obstacleElement) {
        int x = Integer.parseInt(obstacleElement.getElementsByTagName("x").item(0).getTextContent());
        int y = Integer.parseInt(obstacleElement.getElementsByTagName("y").item(0).getTextContent());
        int width = Integer.parseInt(obstacleElement.getElementsByTagName("width").item(0).getTextContent());
        return new Obstacle(x, y, width == 150 ? 1 : 0);
    }

    private static ArrayList<Robot> getRobots(Element gameWindowNode) {
        ArrayList<Robot> robots = new ArrayList<>();
        Element robotsElement = (Element) gameWindowNode.getElementsByTagName("Robots").item(0);
        NodeList robotNodes = robotsElement.getElementsByTagName("Robot");
        for (int i = 0; i < robotNodes.getLength(); i++){
            Element robotElement = (Element) robotNodes.item(i);
            Robot robot = getRobot(robotElement);
            robots.add(robot);
        }
        return robots;
    }

    private static Robot getRobot(Element robotElement) {
        double x = Double.parseDouble(robotElement.getElementsByTagName("x").item(0).getTextContent());
        double y = Double.parseDouble(robotElement.getElementsByTagName("y").item(0).getTextContent());
        double direction = Double.parseDouble(robotElement.getElementsByTagName("direction").item(0).getTextContent());
        boolean selected = Integer.parseInt(robotElement.getElementsByTagName("selected").item(0).getTextContent()) == 1;
        return new Robot(x, y, direction, selected);
    }

    private static Target getTarget(Element gameWindowNode){
        Element targetElement = (Element) gameWindowNode.getElementsByTagName("Target").item(0);
        int target_x = Integer.parseInt(targetElement.getElementsByTagName("x").item(0).getTextContent());
        int target_y = Integer.parseInt(targetElement.getElementsByTagName("y").item(0).getTextContent());
        return new Target(target_x, target_y);
    }

    private static Rectangle getWindowRectangle(Element element){
        int x = Integer.parseInt(element.getElementsByTagName("x").item(0).getTextContent());
        int y = Integer.parseInt(element.getElementsByTagName("y").item(0).getTextContent());
        int width = Integer.parseInt(element.getElementsByTagName("width").item(0).getTextContent());
        int height = Integer.parseInt(element.getElementsByTagName("height").item(0).getTextContent());
        return new Rectangle(x, y, width, height);
    }

    private static void checkConfigValidity() throws ParserConfigurationException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        builder.setErrorHandler(new SimpleErrorHandler());
        try{
            builder.parse(new InputSource("config.xml"));
        }
        catch (SAXException |IOException | NullPointerException e){
            System.out.println("An error occured with Config file, it will be filled with default values.");
            fillConfigWithDefaultValues();
        }
    }

    private static void clearConfig() throws FileNotFoundException {
        PrintWriter pw = new PrintWriter("config.xml");
        pw.write("");
        pw.close();
    }

    private static void fillConfigWithDefaultValues() throws IOException {
        try(FileInputStream fin= new FileInputStream("defaultConfig.txt");
            FileOutputStream fos= new FileOutputStream("config.xml")){
            byte[] buffer = new byte[fin.available()];
            fin.read(buffer, 0, buffer.length);
            fos.write(buffer, 0, buffer.length);
        }
    }
}