package log;

public class LogEntry {
    private LogLevel m_logLevel;
    private String m_strMessage;

    public LogEntry(LogLevel logLevel, String strMessage) {
        m_strMessage = strMessage;
        m_logLevel = logLevel;
    }

    public String getMessage() {
        return m_strMessage;
    }

    public LogLevel getLevel() {
        return m_logLevel;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LogEntry) {
            LogEntry log = (LogEntry) obj;
            return log.m_logLevel == m_logLevel && log.m_strMessage.equals(m_strMessage);
        }
        else
            return false;
    }

    @Override
    public int hashCode() {
        return m_strMessage.hashCode() + m_logLevel.hashCode();
    }
}