package gui;

import models.GameModel;
import models.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class GameWindow extends JInternalFrame {
    private GameVisualizer visualizer;
    private JMenuBar menuBar = new JMenuBar();
    private ArrayList<JToggleButton> buttons = new ArrayList<>();

    public GameWindow(int x, int y, GameModel model) {
        super("Game screen", true, true, true, true);
        visualizer = new GameVisualizer(model);

        createButton("Create Robot", MenuItem.CreateRobot);
        createButton("Choose Robot", MenuItem.SelectRobot);
        createButton("Remove Robot", MenuItem.RemoveRobot);
        createButton("Create Obstacle", MenuItem.CreateObstacle);
        createButton("Remove Obstacle", MenuItem.RemoveObstacle);
        createGraphButton("Show Graph");

        setLocation(x, y);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(visualizer, BorderLayout.CENTER);
        getContentPane().add(panel);
        setJMenuBar(menuBar);
        pack();
    }

    public GameModel getGameModel(){
        return visualizer.model;
    }

    private void createButton(String name, MenuItem item) {
        JToggleButton button = new JToggleButton(name);
        button.addActionListener((event) -> {
            if (button.isSelected()) {
                deselectAllButtonsExceptGraph(button);
                visualizer.model.changeItem(item);
            } else {
                visualizer.model.changeItem(MenuItem.NoneItem);
            }
        });
        buttons.add(button);
        menuBar.add(button);
    }

    private void createGraphButton(String name) {
        JToggleButton button = new JToggleButton(name);
        button.addActionListener((event) -> {
            if (button.isSelected()) {
                visualizer.model.showGraph = true;
            } else {
                visualizer.model.showGraph = false;
                visualizer.model.changeItem(MenuItem.NoneItem);
            }
        });
        buttons.add(button);
        menuBar.add(button);
    }

    private void deselectAllButtonsExceptGraph(JToggleButton currentButton) {
        for (JToggleButton button: buttons){
            if (button.equals(currentButton) || button.equals(buttons.get(buttons.size() - 1)))
                continue;
            button.setSelected(false);
        }
    }
}