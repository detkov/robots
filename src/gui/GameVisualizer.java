package gui;

import models.GameModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Timer;
import java.util.TimerTask;

public class GameVisualizer extends JPanel {
    private static Timer initTimer()
    {
        return new Timer("Events Generator", true);
    }
    public volatile GameModel model;

    public GameVisualizer(GameModel gameModel)
    {
        this.model = gameModel;
        Timer m_timer = initTimer();
        m_timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                onRedrawEvent();
            }
        }, 0, 50);
        m_timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                model.updateRobotsState();
                model.updateGraph();
            }
        }, 0, 10);
        addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                model.HandleClick(e);
                repaint();
            }
        });
        setDoubleBuffered(true);
    }

    private void onRedrawEvent()
    {
        EventQueue.invokeLater(this::repaint);
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        model.drawGameObjects(g2d);
    }
}