package gui;

import javafx.util.Pair;
import log.Config;
import log.Logger;
import models.GameModel;
import models.Window;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;

public class MainApplicationFrame extends JFrame {
    private final JDesktopPane desktopPane = new JDesktopPane();
    private LogWindow log;
    private ArrayList<GameWindow> games = new ArrayList<>();
    private Rectangle stdGameScreen = new Rectangle(200, 0, 400, 400);
    private Rectangle stdLogScreen = new Rectangle(0, 0, 200, 993);
    private GameModel stdGameModel = new GameModel();

    public MainApplicationFrame() throws ParserConfigurationException, SAXException, IOException {
        int inset = 0;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = screenSize.width;
        int height = screenSize.height;
        setBounds(inset, inset, width  - inset * 2, height - inset * 2);
        setContentPane(desktopPane);

        Pair<Rectangle, ArrayList<Pair<Rectangle, GameModel>>> config = Config.readConfig();
        log = (LogWindow) createWindow(Window.Log, config.getKey(), null);
        for (Pair<Rectangle, GameModel> gameWindowAndModel : config.getValue())
            games.add((GameWindow) createWindow(Window.Game, gameWindowAndModel.getKey(), gameWindowAndModel.getValue()));

        setJMenuBar(generateMenuBar());
        ListenForQuit();
    }

    private void ListenForQuit() {
        setDefaultCloseOperation(JInternalFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                UIManager.put("OptionPane.yesButtonText", "Yes");
                UIManager.put("OptionPane.noButtonText", "No");
                int result = JOptionPane.showConfirmDialog(desktopPane, "Are you sure you want to quit?",
                        "Confirmation", JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                    try {
                        removeClosedScreens();
                        Config.writeConfig(log, games, getGameModels());
                    } catch (TransformerException | IOException | ParserConfigurationException ignored) { }
                    System.exit(0);
                }
            }
        });
    }

    private ArrayList<GameModel> getGameModels(){
        ArrayList<GameModel> models = new ArrayList<>();
        for (GameWindow game : games)
            models.add(game.getGameModel());
        return models;
    }

    private void removeClosedScreens(){
        ArrayList<GameWindow> gameWindows = (ArrayList<GameWindow>) games.clone();
        for (int i = 0; i < gameWindows.size(); i++)
            if (gameWindows.get(i).isClosed())
                games.remove(gameWindows.get(i));
        if (games.isEmpty())
            games.add((GameWindow) createWindow(Window.Game, stdGameScreen, stdGameModel));
        if (log.isClosed())
            log = (LogWindow) createWindow(Window.Log, stdLogScreen, null);
    }

    private JInternalFrame createWindow(Window window, Rectangle rect, GameModel model) {
        JInternalFrame frame = null;
        switch (window) {
            case Log:
                frame = new LogWindow(Logger.getDefaultLogSource(), rect.x, rect.y);
                frame.setSize(rect.width,rect.height);
                break;
            case Game:
                frame = new GameWindow(rect.x, rect.y, model);
                frame.setSize(rect.width,rect.height);
                break;
        }
        addWindow(frame);
        return frame;
    }

    private void addWindow(JInternalFrame frame) {
        desktopPane.add(frame);
        frame.setVisible(true);
    }

    private JMenuBar generateMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu lookAndFeelMenu = generateMenu("Display Mode", KeyEvent.VK_V, "Controlling the application display mode");
        lookAndFeelMenu.add(generateSchemeItem("System Look", UIManager.getSystemLookAndFeelClassName()));
        lookAndFeelMenu.add(generateSchemeItem("CrossPlatform Look", UIManager.getCrossPlatformLookAndFeelClassName()));

        JMenu testMenu = generateMenu("Tests", KeyEvent.VK_T, "Test commands");
        JMenuItem addLogMessageItem = new JMenuItem("Message to Log", KeyEvent.VK_S);
        addLogMessageItem.addActionListener((event) -> Logger.debug("New line"));
        testMenu.add(addLogMessageItem);

        JMenu gameFieldMenu = generateMenu("Game screen", KeyEvent.VK_T, "Controlling the application game screens");
        JMenuItem addNewGameScreen = new JMenuItem("Add new game screen", KeyEvent.VK_S);
        addNewGameScreen.addActionListener((event) -> games.add((GameWindow) createWindow(Window.Game, stdGameScreen, stdGameModel)));
        gameFieldMenu.add(addNewGameScreen);

        menuBar.add(lookAndFeelMenu);
        menuBar.add(testMenu);
        menuBar.add(gameFieldMenu);

        return menuBar;
    }

    public static JMenu generateMenu(String name, int key, String description) {
        JMenu menu = new JMenu(name);
        menu.setMnemonic(key);
        menu.getAccessibleContext().setAccessibleDescription(description);
        return menu;
    }

    private JMenuItem generateSchemeItem(String name, String className){
        JMenuItem item = new JMenuItem(name, KeyEvent.VK_S);
        item.addActionListener((event) -> {
            setLookAndFeel(className);
            this.invalidate();
        });
        return item;
    }

    private void setLookAndFeel(String className) {
        try {
            UIManager.setLookAndFeel(className);
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | UnsupportedLookAndFeelException ignored)
        { }
    }
}