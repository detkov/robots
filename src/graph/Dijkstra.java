package graph;

import java.awt.*;
import java.util.ArrayList;
import java.util.Stack;

import static java.util.Arrays.fill;

public class Dijkstra {
    static int INF = Integer.MAX_VALUE / 2; // "Бесконечность"
    private int vNum; // количество вершин
    private MultiList graph; // описание графа

    public Dijkstra(MultiList list){
        this.graph = list;
        this.vNum = list.head.length;
    }

    public ArrayList<Point> dijkstraRMQ(int start, int end) {
        boolean[] used = new boolean[vNum]; // массив пометок
        int[] prev = new int[vNum]; // массив предков
        double[] dist = new double[vNum]; // массив расстояний
        RMQ rmq = new RMQ(vNum); // RMQ

        /* Инициализация */
        fill(prev, -1);
        fill(dist, INF);
        rmq.set(start, dist[start] = 0);

        for (; ; ) {
            int v = rmq.minIndex(); // выбираем ближайшую вершину
            if (v == -1 || v == end) break; // если она не найдена, или является конечной, то выходим

            used[v] = true; // помечаем выбранную вершину
            rmq.set(v, INF); // и сбрасываем ее значение в RMQ

            for (int i = graph.head[v]; i != 0; i = graph.next[i]) { // проходим по смежным вершинам
                int nv = graph.vert[i];
                double cost = graph.cost[i];
                if (!used[nv] && dist[nv] > dist[v] + cost) { // если можем улучшить оценку расстояния
                    rmq.set(nv, dist[nv] = dist[v] + cost); // улучшаем ее
                    prev[nv] = v; // помечаем предка
                }
            }
        }

        /* Восстановление пути */
        Stack stack = new Stack();
        for (int v = end; v != -1; v = prev[v]) {
            stack.push(v);
        }
        int[] sp = new int[stack.size()];
        for (int i = 0; i < sp.length; i++)
            sp[i] = Integer.parseInt(String.valueOf(stack.pop()));

        /* Вывод результата */
//        System.out.printf("Кратчайшее расстояние между %d и %d = %f\n", start, end, dist[end]);
//        System.out.println("Кратчайший путь: " + Arrays.toString(sp));
        ArrayList<Point> nodes = new ArrayList<>();
        for (int node : sp){
            Point currentNode = graph.reverseDictionary.get(node);
            nodes.add(currentNode);
        }
//        System.out.printf("Кратчайшее расстояние между %d и %d = %f\n", start, end, dist[end]);
//        System.out.println("Кратчайший путь: " + nodes.stream().map(Object::toString).collect(Collectors.joining(", ")));
        return nodes;
    }
}