package graph;

import javafx.util.Pair;
import models.GameModel;
import models.Obstacle;
import models.Robot;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import static models.Utilities.distance;
import static models.Utilities.round;

public class Graph {
    public static ArrayList<Point> getGraphNodes(ArrayList<Obstacle> obstacles, Robot robot, Point target){
        ArrayList<Point> innerNodes = new ArrayList<>();
        for (Obstacle obstacle : obstacles){
            ArrayList<Obstacle> obstaclesToCheck = new ArrayList<>(obstacles);
            obstaclesToCheck.remove(obstacle);
            for (Obstacle anotherObstacle : obstaclesToCheck){
                if (obstacle.rect.contains(anotherObstacle.leftUpper))
                    innerNodes.add(anotherObstacle.leftUpper);
                if (obstacle.rect.contains(anotherObstacle.rightUpper))
                    innerNodes.add(anotherObstacle.rightUpper);
                if (obstacle.rect.contains(anotherObstacle.leftLower))
                    innerNodes.add(anotherObstacle.leftLower);
                if (obstacle.rect.contains(anotherObstacle.rightLower))
                    innerNodes.add(anotherObstacle.rightLower);
            }
        }

        ArrayList<Point> outerNodes = new ArrayList<>();
        if(robot != GameModel.defaultRobot)
            outerNodes.add(new Point(round(robot.x), round(robot.y)));
        for (Obstacle obstacle : obstacles){
            outerNodes.add(obstacle.leftUpper);
            outerNodes.add(obstacle.rightUpper);
            outerNodes.add(obstacle.leftLower);
            outerNodes.add(obstacle.rightLower);
        }
        outerNodes.removeAll(innerNodes);
        outerNodes.add(target);

        return outerNodes;
    }

    public static ArrayList<Pair<Line2D, Double>> getGraphEdges(ArrayList<Point> nodes, ArrayList<Obstacle> obstacles){
        ArrayList<Pair<Line2D, Double>> edges = new ArrayList<>();
        for(Point node : nodes){
            ArrayList<Point> nodesToCheck = new ArrayList<>(nodes);
            nodesToCheck.remove(node);
            for(Point nodeToCheck : nodesToCheck){
                boolean intersects = false;
                for(Obstacle obstacle : obstacles){
                    Line2D edge = new Line2D.Double(node, nodeToCheck);
                    if (edge.intersects(obstacle.rect)){
                        intersects = true;
                        break;
                    }
                }
                if (!intersects){
                    Line2D edge = new Line2D.Double(node, nodeToCheck);
                    Double distance = distance(node.x, node.y, nodeToCheck.x, nodeToCheck.y);
                    edges.add(new Pair<>(edge, distance));
                }
            }
        }
        return edges;
    }
}
