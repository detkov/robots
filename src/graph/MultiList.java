package graph;

import javafx.util.Pair;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MultiList {
    int[] head;
    int[] next;
    int[] vert;
    double[] cost;
    private int cnt = 1;
    private Map<Point, Integer> dictionary;
    public Map<Integer, Point> reverseDictionary;


    public MultiList(ArrayList<Point> nodes, ArrayList<Pair<Line2D, Double>> edges) {
        head = new int [nodes.size()];
        next = new int [edges.size() * 2 + 1];
        vert = new int [edges.size() * 2 + 1];
        cost = new double [edges.size() * 2 + 1];
        dictionary = new HashMap<>();
        for (int i = 0; i < nodes.size(); i++) {
            dictionary.put(nodes.get(i), i);
        }
        reverseDictionary = new HashMap<>();
        for (int i = 0; i < dictionary.size(); i++) {
            reverseDictionary.put(i, nodes.get(i));
        }

        for (Pair<Line2D, Double> edge : edges){
            Line2D line = edge.getKey();
            int p1 = dictionary.get(line.getP1());
            int p2 = dictionary.get(line.getP2());
            add(p1, p2, edge.getValue());
        }
    }

    void add(int u, int v, double w) {
        next[cnt] = head[u];
        vert[cnt] = v;
        cost[cnt] = w;
        head[u] = cnt++;
        next[cnt] = head[v];
        vert[cnt] = u;
        cost[cnt] = w;
        head[v] = cnt++;
    }
}
